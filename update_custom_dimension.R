update_custom_dimension <- function(custom_dimension_id,
                                    web_property_id,
                                    account_id,
                                    cd_name = F,
                                    cd_scope = F,
                                    cd_is_active = F,
                                    ec = F,
                                    proxy_usage = F){
  source("~/R/rgamgmt/proto_request.R")
  method <- paste0("/management/accounts/", account_id, "/webproperties/", web_property_id, "/customDimensions/", custom_dimension_id)
  payload <- toJSON(list(
    name = ifelse(identical(cd_name, F), NULL, cd_name),
    scope = ifelse(identical(cd_scope, F), NULL, cd_scope),
    active = cd_is_active
  ), auto_unbox = T)
  x <- proto_request(method, payload, "analytics.edit", httr::PATCH, proxy_usage, ec)
  x
}